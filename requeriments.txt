numpy==1.18.3
opencv-python==4.2.0.34
pickle4==0.0.1
pybase64==1.0.1
torch==1.5.0
requests==2.23.0
torchvision==0.6.0