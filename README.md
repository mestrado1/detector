# RetinaFace in PyTorch

A [PyTorch](https://pytorch.org/) implementation of [RetinaFace: Single-stage Dense Face Localisation in the Wild](https://arxiv.org/abs/1905.00641). Model size only 1.7M, when Retinaface use mobilenet0.25 as backbone net. We also provide resnet50 as backbone net to get better result. The official code in Mxnet can be found [here](https://github.com/deepinsight/insightface/tree/master/RetinaFace).


## Instalação
1. Clonar o repositório git
    ```bash
    # repositório git
    $ git clone https://gitlab.com/mestrado1/identifier.git
    ```
2. Intalar os requerimentos
    ```bash
    # Requerimentos
    $ pip install -r requirements.txt
    ```
3. Rodar código
    ```bash
    # Executar
    $ Python3 teste.py
    ```
## Funcionamento
  
  >O algoritmo recebe imagens de uma câmera através do protocolo RTSP e  extrai as faces.

  > É necessário mudar o endereço da câmera para a captura de imagens: CAMERA_PRINCIPAL = 'rtsp://192.168.1.4:5554/camera'.

  > As imagens são enviadas para uma API que irá armazená-las até que as mesmas sejam processadas. 
  A API pode ser encontrada em: https://gitlab.com/mestrado1/interoperator.git

  > Este código de teste utiliza a execução da API local. Se o usuário desejar utilizar uma API em outro ambiente será necessário mudar o endereço de: API_ENDPOINT = 'http://localhost:5000/images'.


## References
- [Retina in Pytorch](https://github.com/biubug6/Pytorch_Retinaface)
- [FaceBoxes](https://github.com/zisianw/FaceBoxes.PyTorch)
- [Retinaface (mxnet)](https://github.com/deepinsight/insightface/tree/master/RetinaFace)
```
@inproceedings{deng2019retinaface,
title={RetinaFace: Single-stage Dense Face Localisation in the Wild},
author={Deng, Jiankang and Guo, Jia and Yuxiang, Zhou and Jinke Yu and Irene Kotsia and Zafeiriou, Stefanos},
booktitle={arxiv},
year={2019}
```
